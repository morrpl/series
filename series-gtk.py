#!/usr/bin/env python

import re

import glib
import gtk
import pygtk

pygtk.require("2.0")


class Series:
    series = {}
    chosen_series = ""
    DEFAULT_FILENAME = "series.txt"
    default_browser = "google-chrome-stable"
    default_browser_private = "--private"

    def __init__(self):
        VERSION = "0.1"

        titles = ["Title", "Season/episode"]

        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_title("series gtk")
        self.window.set_usize(400, 400)
        self.window.connect("delete_event", self.delete_event)
        vbox = gtk.VBox(gtk.FALSE, 5)
        vbox.set_border_width(5)
        self.window.add(vbox)
        # Create a scrolled window to pack the CList widget into
        scrolled_window = gtk.ScrolledWindow()
        scrolled_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_ALWAYS)

        vbox.pack_start(scrolled_window, gtk.TRUE, gtk.TRUE, 0)

        # Create the CList. For this example we use 2 columns
        clist = gtk.CList(2, titles)
        clist.set_shadow_type(gtk.SHADOW_OUT)
        scrolled_window.add(clist)
        clist.set_column_width(0, 200)
        clist.connect("select_row", self.selection_made)

        hbox = gtk.HBox(gtk.FALSE, 0)
        hbox_torrent = gtk.HBox(gtk.FALSE, 0)
        vbox.pack_start(hbox, gtk.FALSE, gtk.TRUE, 0)
        vbox.pack_start(hbox_torrent, gtk.FALSE, gtk.TRUE, 0)

        button_inc = gtk.Button("Increase")
        button_season = gtk.Button("New season")
        button_torrent = gtk.Button("Torrent")
        button_torrent_all = gtk.Button("Torrent All")

        hbox.pack_start(button_inc, gtk.TRUE, gtk.TRUE, 0)
        hbox.pack_start(button_season, gtk.TRUE, gtk.TRUE, 0)
        hbox.pack_start(button_torrent, gtk.TRUE, gtk.TRUE, 0)
        hbox_torrent.pack_start(button_torrent_all, gtk.TRUE, gtk.TRUE, 0)
        button_inc.connect_object("clicked", self.button_inc_clicked, clist)
        button_season.connect_object("clicked", self.button_season_clicked, clist)
        button_torrent.connect_object("clicked", self.button_torrent_clicked, clist)
        button_torrent_all.connect_object(
            "clicked", self.button_torrent_all_clicked, clist
        )

        series = self.read_series(self.DEFAULT_FILENAME)
        for i in sorted(series.keys()):
            clist.append([i, series[i]])

        self.window.show_all()

    def delete_event(self, widget, event, data=None):
        self.write_series(self.DEFAULT_FILENAME)
        gtk.main_quit()

    def selection_made(self, clist, row, column, event, data=None):
        self.chosen_series = clist.get_text(row, 0)
        return

    def refresh_data(self, data):
        data.clear()
        for i in sorted(self.series.keys()):
            data.append([i, self.series[i]])

    def button_inc_clicked(self, data):
        if not self.chosen_series:
            print("Nothing is chosen, try to select something")
        else:
            self.increment(self.chosen_series, False)
            self.refresh_data(data)

    def button_season_clicked(self, data):
        if not self.chosen_series:
            print("Nothing is chosen, try to select something")
        else:
            self.increment(self.chosen_series, True)
            self.refresh_data(data)

    def read_series(self, file):
        with open(file, "r") as f:
            for line in f.readlines():
                (series_name, tmp_number) = line.split(" - ")
                self.series[series_name] = tmp_number.strip()
            return self.series

    def button_torrent_clicked(self, data):
        if not self.chosen_series:
            print("Nothing is chosen, try to select something")
        else:
            glib.spawn_async(
                [
                    self.default_browser,
                    self.default_browser_private,
                    "https://thepiratebay.se/search/" + self.chosen_series + "/0/7/0",
                ],
                flags=glib.SPAWN_SEARCH_PATH
                | glib.SPAWN_STDOUT_TO_DEV_NULL
                | glib.SPAWN_STDOUT_TO_DEV_NULL,
                standard_input=None,
                standard_error=None,
                standard_output=None,
            )

    def button_torrent_all_clicked(self, data):
        for i in sorted(self.series.keys()):
            glib.spawn_async(
                [
                    self.default_browser,
                    self.default_browser_private,
                    "https://thepiratebay.se/search/" + i + "/0/7/0",
                ],
                flags=glib.SPAWN_SEARCH_PATH
                | glib.SPAWN_STDOUT_TO_DEV_NULL
                | glib.SPAWN_STDOUT_TO_DEV_NULL,
                standard_input=None,
                standard_error=None,
                standard_output=None,
            )

    def increment(self, series_name, inc_season=False):
        episode_nr = self.series[series_name]
        (season, episode) = re.findall(r"S(\d+)E(\d+)", episode_nr)[0]
        if inc_season:
            season = int(season) + 1
            episode = 1
        else:
            episode = int(episode) + 1
        series_number = self.make_series_number(season, episode)
        self.series[series_name] = series_number

    def make_series_number(self, season, episode):
        return "S" + str(season).zfill(2) + "E" + str(episode).zfill(2)

    def write_series(self, file):
        with open(file, "w") as f:
            for k in sorted(self.series.keys()):
                f.write(k + " - " + self.series[k] + "\n")

    def main(self):
        gtk.main()


if __name__ == "__main__":
    series = Series()
    series.main()
