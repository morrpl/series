import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="series",
    version="2020-05-30",
    author="Daniel Horecki",
    author_email="morr@morr.pl",
    description="Tracking tv series",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/series",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
