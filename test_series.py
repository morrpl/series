import io

from series import DEFAULT_FILENAME, make_series_number, Series

series = Series()
series_name = "unittest"


def test_make_series_number():
    assert make_series_number(2, 2) == "S02E02"


def test_read_series():
    series.read_series(DEFAULT_FILENAME)
    assert series.list_series["gotham"] == "S01E14"


def test_parse_commands():
    series.parse_commands(["--name", series_name])
    assert series.args.name == series_name


def test_add_series(monkeypatch):
    monkeypatch.setattr("sys.stdin", io.StringIO(u"4\n2\n"))
    series.add_series()


def test_increment():
    print(series)
    series.increment()
    assert series.list_series[series.series_name] == "S04E03"


def test_increment_season():
    series.increment(inc_season=True)
    assert series.list_series[series.series_name] == "S05E01"


if __name__ == "__main__":
    pytest.main()
