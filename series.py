#!/usr/bin/python3

""" Script to handle series.txt file.
Can show, add, remove or reset entries in files.
Used to track TV series.
"""

from __future__ import print_function

import argparse
import re
import sys
from typing import Any, Dict, List

VERSION = "0.1"
DEFAULT_FILENAME = "series.txt"


def make_series_number(season: str, episode: str) -> str:
    """ return series number """
    return "S" + str(season).zfill(2) + "E" + str(episode).zfill(2)


class Series:
    """ main class of file """

    def __init__(self) -> None:
        self.list_series: Dict[str, str] = {}
        self.args: Any = {}
        self.series_name: str = ""

    def parse_commands(self, arguments: List[str]) -> None:
        """ parses arguments """
        parser = argparse.ArgumentParser(
            description="Looks for episode already watched."
        )
        parser.add_argument("--name", type=str, help="name of series")
        parser.add_argument(
            "--inc", action="store_true", help="increment episode number"
        )
        parser.add_argument(
            "--season", action="store_true", help="increment season number"
        )
        parser.add_argument(
            "--reset", action="store_true", help="reset season/episode number"
        )
        parser.add_argument("--remove", action="store_true", help="remove series")
        parser.add_argument(
            "--version", action="version", version="%(prog)s " + VERSION
        )

        self.args = parser.parse_args(arguments)
        self.series_namer = self.args.name

    def main(self) -> None:
        """ main method """
        self.parse_commands(sys.argv[1:])

        try:
            self.list_series = self.read_series(DEFAULT_FILENAME)
        except EnvironmentError:
            print("Cannot read file")

        if self.series_name:
            if self.series_name in self.list_series:
                if self.args.inc:
                    self.increment()
                if self.args.season:
                    self.increment(inc_season=True)
                if self.args.reset:
                    self.add_series()
                if self.args.remove:
                    self.del_series()
                else:
                    self.show_episode()
            else:
                self.add_series()
        else:
            self.show_all_episodes()

        self.write_series(DEFAULT_FILENAME)

    def increment(self, inc_season: bool = False) -> None:
        """ take entry and increment season or
        episode number
        """
        episode_nr = self.list_series[self.series_name]
        (season, episode) = re.findall(r"S(\d+)E(\d+)", episode_nr)[0]
        if inc_season:
            season = int(season) + 1
            episode = 1
        else:
            episode = int(episode) + 1
        series_number = make_series_number(season, episode)
        self.list_series[self.series_name] = series_number

    def add_series(self) -> None:
        """ add entries to list """
        season = input("Enter season number: ")
        episode = input("Enter episode number: ")
        self.list_series[self.series_name] = make_series_number(season, episode)

    def show_episode(self) -> None:
        """ shows episode """
        print(self.series_name, "-", self.list_series[self.series_name])

    def del_series(self) -> None:
        """ remove entry from list """
        self.list_series.pop(self.series_name, None)

    def show_all_episodes(self) -> None:
        """ show all entries """
        for k in sorted(self.list_series.keys()):
            print(k, "-", self.list_series[k])

    def write_series(self, file: str) -> None:
        """ write entries to file """
        with open(file, "w") as entries:
            for k in sorted(self.list_series.keys()):
                entries.write(k + " - " + self.list_series[k] + "\n")

    def read_series(self, file: str) -> Dict:
        """ read all entries from file """
        with open(file, "r") as entries:
            for line in entries.readlines():
                (series_name, tmp_number) = line.split(" - ")
                self.list_series[series_name] = tmp_number.strip()
            return self.list_series


if __name__ == "__main__":
    SERIES = Series()
    SERIES.main()
